/**
 * @typedef {{
 * nev: string;
 * felaras: boolean;
 * }} Etel
 */

/**
 * @typedef {{
 * mai: Etel[]
 * holnapi: Etel[]
 * }} ApiResponse
 */
/** @type {import('./$types').PageLoad} */
export async function load({ fetch }) {
	const result = await fetch('https://melodout.vercel.app/api/maholnap');
	/** @type {ApiResponse} */
	const data = await result.json();
	return data;
}
export const prerender = true;
