from datetime import date, datetime
from http.server import BaseHTTPRequestHandler
import requests as rq
from bs4 import BeautifulSoup
import json


def getSoup(url):
    s = rq.Session()
    s.headers['User-Agent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36'
    r = s.get(url, allow_redirects=True)
    _html = r.text
    soup = BeautifulSoup(_html, features="html.parser")
    return soup


def get_days(soup):
    menu = soup.select_one('div.menu-type-36')
    submenu = menu.select_one('div.menu')
    napimenuk = menu.find_all('div', {'class', 'menu'}, {'class', 'col-sm-7'})
    return napimenuk


def getkajak(napi):
    return napi.find_all('div', {'class', 'item'})


def getKajaDetails(kaja):
    nev = kaja.select_one('.name')
    arak = kaja.find_all('span', {'class', 'price'})
    felaras = False if [ar.text for ar in arak][-1] == "" else True
    return {"nev": nev.text, "felaras": felaras}


class handler(BaseHTTPRequestHandler):

    def do_GET(self):
        time = datetime.now()
        soup = getSoup('http://www.melodin.hu/melodin-dock-105?qr')
        napimenuk = get_days(soup)
        napok = napimenuk[0].find_all('div', {'class', 'col-sm-7'})
        ma = date.today().weekday()
        if ma==4:
            # It's friday madafaka
            print('élj a mának')
            bigdata = {"mai": []}
            maikajak = getkajak(napok[ma])

            for kaja in maikajak:
                bigdata['mai'].append(getKajaDetails(kaja))

        elif ma>4:
            print("hétvége van megacsill")
            bigdata = {"weekend": []}
        else:
            bigdata = {"mai": []}
            bigdata["holnapi"]=[]
            maikajak = getkajak(napok[ma])
            holnapikajak = getkajak(napok[ma+1])
            for kaja in holnapikajak:
                bigdata['holnapi'].append(getKajaDetails(kaja))
            for kaja in maikajak:
                bigdata['mai'].append(getKajaDetails(kaja))

        print('took', datetime.now()-time, 'time')

        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Cache-Control', 'max-age=3600, public')
        self.end_headers()
        self.wfile.write(json.dumps(bigdata).encode('utf-8'))
        return
